# omnivore-cli

A command line interface for managing [Omnivore feeds](https://omnivore.app).

`omnivore-cli` was created to make it easier to organize feeds in omnivore in categories. If you are coming from other reeader apps like [Feedly](https://feedly.com), you might be used to organizing your feeds in categories. `omnivore-cli` allows you to do the same.

## Features

- category management
  - add categories
  - remove categories
  - list categories & their feeds
- feed management
  - add feeds
  - remove feeds
  - list feeds
- OPML import
  - import feeds and their categories from an OPML file (currently only Feedly OPML structure is supported with categories)

## Installation

TODO add brew instructions

## Usage

To interact with `omnivore-cli`, an Omnivore API key is required. You can generate your own API key by [following the official instructions](https://docs.omnivore.app/integrations/api.html#getting-an-api-token).

Once you have your API key, you can set it as an environment variable:

```bash
export OMNIVORE_API_KEY=your-api-key
```

Then you can start using `omnivore-cli` by adding your first category:

```bash
omnivore-cli category add "Photography"
```
![category add](./images/example-feed-add.png)
