package main

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/msvechla/omnivore-cli/pkg/omnivoreapi"
)

const (
	feedCategoryLabelPrefix = "feeds: "
)

// removeCategory removes a category for feeds
func removeCategory(ctx context.Context, apiKey string, endpoint string, categoryName *string, debug bool) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}
	if categoryName == nil {
		categories, err := getCategoryLabels(ctx, client)
		if err != nil {
			return err
		}
		cat, err := promptForCategory("Category to remove", categories)
		if err != nil {
			return err
		}
		categoryName = &cat
	}

	catLabel, err := client.GetLabelFromCache(ctx, feedCategoryLabel(*categoryName))
	if err != nil {
		return err
	}

	_, errCodes, err := client.DeleteLabel(ctx, catLabel.ID)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("errors: %v", errCodes)
	}
	fmt.Printf("Category %s removed\n", *categoryName)
	return nil
}

// addCategory adds a category for feeds
func addCategory(ctx context.Context, apiKey string, endpoint string, categoryName string, labelColor string, debug bool) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}

	_, updated, err := client.EnsureLabel(ctx, omnivoreapi.CreateLabelInput{
		Name:        feedCategoryLabel(categoryName),
		Color:       &labelColor,
		Description: &categoryName,
	})
	if err != nil {
		return err
	}

	updateSearch, err := createSavedSearchForCategory(ctx, client, categoryName)
	if err != nil {
		return err
	}

	if updated || updateSearch {
		fmt.Printf("Category %s created\n", categoryName)
	} else {
		fmt.Printf("Category %s already exists\n", categoryName)
	}
	return nil
}

// listCategories lists the categories of the feeds
func listCategories(ctx context.Context, apiKey string, endpoint string, debug bool) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}

	categoryLabels, err := getCategoryLabels(ctx, client)
	if err != nil {
		return err
	}

	type category struct {
		Name     string `json:"name"`
		NumFeeds int    `json:"numFeeds"`
	}

	categories := []category{}
	for categoryName := range categoryLabels {
		rules, rulesErr := findRulesForCategory(ctx, client, categoryName)
		if rulesErr != nil {
			return err
		}
		categories = append(categories, category{
			Name:     categoryName,
			NumFeeds: len(rules),
		})
	}

	d, err := json.MarshalIndent(categories, "", "  ")
	if err != nil {
		return err
	}
	fmt.Println(string(d))
	return nil
}

func findRulesForCategory(ctx context.Context, client *omnivoreapi.Client, category string) ([]omnivoreapi.Rule, error) {
	rules, err := client.GetRulesFromCache(ctx)
	if err != nil {
		return nil, err
	}
	categoryLabel := feedCategoryLabel(category)
	categoryRules := []omnivoreapi.Rule{}

	labels, err := client.GetLabelsFromCache(ctx)
	if err != nil {
		return nil, err
	}
	labelIDCache := map[omnivoreapi.ID]omnivoreapi.Label{}
	for _, label := range labels {
		labelIDCache[label.ID] = label
	}

	for _, rule := range rules {
		for _, action := range rule.Actions {
			if action.Type == omnivoreapi.RuleActionTypeAddLabel {
				for _, labelID := range action.Params {
					label, ok := labelIDCache[omnivoreapi.ID(labelID)]
					if !ok {
						log.Debugf("label %s not found in cache during category check", labelID)
						continue
					}
					if label.Name == categoryLabel {
						categoryRules = append(categoryRules, rule)
					}
				}
			}
		}
	}
	return categoryRules, nil
}

// getCategoryLabels returns a map of category names to labels
func getCategoryLabels(ctx context.Context, client *omnivoreapi.Client) (map[string]omnivoreapi.Label, error) {
	labels, err := client.GetLabelsFromCache(ctx)
	if err != nil {
		return nil, err
	}
	categoryLabels := map[string]omnivoreapi.Label{}
	for name, label := range labels {
		if strings.HasPrefix(name, feedCategoryLabelPrefix) {
			categoryLabels[feedCategoryNameFromLabel(name)] = label
		}
	}
	return categoryLabels, nil
}

// createLabelRuleForFeedAndCategory creates a label and a rule for a feed and category
func createLabelRuleForFeedAndCategory(ctx context.Context, client *omnivoreapi.Client, rule Rule) (bool, error) {
	labelIDs := []string{}
	for _, label := range rule.Labels {
		l, err := client.GetLabelFromCache(ctx, label)
		if err != nil {
			return false, err
		}
		labelIDs = append(labelIDs, string(l.ID))
	}

	_, updated, err := client.EnsureRule(ctx, omnivoreapi.SetRuleInput{
		Name:        rule.Name,
		Filter:      rule.Filter,
		Description: &rule.Name,
		Enabled:     true,
		Actions: []omnivoreapi.RuleActionInput{
			{
				Type:   omnivoreapi.RuleActionTypeAddLabel,
				Params: labelIDs,
			},
		},
		EventTypes: []omnivoreapi.RuleEventType{omnivoreapi.RuleEventPageCreated},
	})
	if err != nil {
		return false, err
	}
	return updated, nil
}

func createSavedSearchForCategory(ctx context.Context, client *omnivoreapi.Client, category string) (bool, error) {
	cat := omnivoreapi.FilterCategorySearch
	_, updated, err := client.EnsureFilter(
		ctx,
		omnivoreapi.SaveFilterInput{
			Name:        category,
			Filter:      categoryLabelFilter(category),
			Description: savedSearchFilterDescription(category),
			Category:    &cat,
		},
	)
	if err != nil {
		return updated, fmt.Errorf("failed to save filter: %w", err)
	}
	return updated, nil
}

func savedSearchFilterDescription(category string) *string {
	desc := fmt.Sprintf("all feeds in category %s", category)
	return &desc
}

func categoryLabelFilter(category string) string {
	return fmt.Sprintf("label:\"%s\"", feedCategoryLabel(category))
}

// feedCategoryLabel returns the label for a feed category
func feedCategoryLabel(category string) string {
	return fmt.Sprintf("%s%s", feedCategoryLabelPrefix, category)
}

// feedCategoryNameFromLabel returns the category name from a feed category label
func feedCategoryNameFromLabel(label string) string {
	return strings.TrimPrefix(label, feedCategoryLabelPrefix)
}
