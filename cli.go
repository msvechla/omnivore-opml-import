package main

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	// nolint: gosec
	flagOmnivoreAPIKey = "omnivore-api-key"
	flagOmnivoreAPIURL = "omnivore-api-url"
	flagDebug          = "debug"
	flagLabelColor     = "label-color"
	usageLabelColor    = "hex color of the label for the imported feed categories"
	defaultLabelColor  = "#32A885"
)

func setupCLI() *cli.App {
	app := &cli.App{
		Name:  "omnivore",
		Usage: "CLI tool for managing feed subscriptions",
		Commands: []*cli.Command{
			feedCommand(),
			linkCommand(),
			importCommand(),
			categoryCommand(),
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     flagOmnivoreAPIKey,
				Usage:    "Omnivore API key",
				EnvVars:  []string{cliFlagToEnvVar(flagOmnivoreAPIKey)},
				Required: true,
			},
			&cli.StringFlag{
				Name:    flagOmnivoreAPIURL,
				Usage:   "Omnivore API URL",
				EnvVars: []string{cliFlagToEnvVar(flagOmnivoreAPIURL)},
				Value:   "https://api-prod.omnivore.app/api/graphql",
			},
			&cli.BoolFlag{
				Name:    flagDebug,
				Usage:   "enable debug logging",
				EnvVars: []string{cliFlagToEnvVar(flagDebug)},
				Value:   false,
				Action: func(_ *cli.Context, debug bool) error {
					if debug {
						log.SetLevel(log.DebugLevel)
					}
					return nil
				},
			},
		},
	}
	return app
}

func cliFlagToEnvVar(flagName string) string {
	return strings.ReplaceAll(strings.ToUpper(flagName), "-", "_")
}

func categoryCommand() *cli.Command {
	return &cli.Command{
		Name:    "category",
		Aliases: []string{"c"},
		Usage:   "manage categories",
		Subcommands: []*cli.Command{
			{
				Name:  "add",
				Usage: "add a new category",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "name",
						Usage:    "name of the category",
						Required: true,
					},
					&cli.StringFlag{
						Name:     flagLabelColor,
						Usage:    usageLabelColor,
						Required: false,
						Value:    defaultLabelColor,
					},
				},
				Action: func(c *cli.Context) error {
					return addCategory(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.String("name"), c.String(flagLabelColor), c.Bool(flagDebug))
				},
			},
			{
				Name:  "list",
				Usage: "list categories",
				Action: func(c *cli.Context) error {
					return listCategories(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.Bool(flagDebug))
				},
			},
			{
				Name:  "remove",
				Usage: "remove a category",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "name",
						Usage: "name of the category",
					},
				},
				Action: func(c *cli.Context) error {
					var categoryName *string
					if c.IsSet("name") {
						name := c.String("name")
						categoryName = &name
					}
					return removeCategory(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), categoryName, c.Bool(flagDebug))
				},
			},
		},
	}
}

func importCommand() *cli.Command {
	return &cli.Command{
		Name:    "import",
		Aliases: []string{"i"},
		Usage:   "import feed subscriptions",
		Subcommands: []*cli.Command{
			{
				Name:  "opml",
				Usage: "import feed subscriptions from an OPML file",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "file",
						Usage:    "path to the OPML file",
						Required: true,
					},
					&cli.StringFlag{
						Name:     flagLabelColor,
						Usage:    usageLabelColor,
						Required: false,
						Value:    defaultLabelColor,
					},
					&cli.BoolFlag{
						Name:     "create-categories",
						Usage:    "create categories using saved searches and rules to label the feeds from the OPML file",
						Required: false,
						Value:    true,
					},
					&cli.BoolFlag{
						Name:     "",
						Usage:    "create categories using saved searches and rules to label the feeds from the OPML file",
						Required: false,
						Value:    true,
					},
				},
				Action: func(c *cli.Context) error {
					return importOPMLFile(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.Bool(flagDebug), c.String("file"), c.Bool("create-categories"), c.String(flagLabelColor))
				},
			},
		},
	}
}

func linkCommand() *cli.Command {
	return &cli.Command{
		Name:    "link",
		Aliases: []string{"l"},
		Usage:   "manage individual links",
		Subcommands: []*cli.Command{
			{
				Name:  "add",
				Usage: "add a new individual link to a feed subscription",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "feed",
						Usage: "name of the feed to add the link",
					},
					&cli.StringFlag{
						Name:  "url",
						Usage: "URL of the link to add",
					},
				},
				Action: func(c *cli.Context) error {
					fmt.Println("Adding link...")
					return nil
				},
			},
			{
				Name:  "remove",
				Usage: "remove an individual link from a feed subscription",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "feed",
						Usage: "name of the feed to remove the link from",
					},
					&cli.StringFlag{
						Name:  "url",
						Usage: "URL of the link to remove",
					},
				},
				Action: func(c *cli.Context) error {
					fmt.Println("Removing link...")
					return nil
				},
			},
		},
	}
}

func feedCommand() *cli.Command {
	return &cli.Command{
		Name:    "feed",
		Aliases: []string{"f"},
		Usage:   "manage feed subscriptions",
		Subcommands: []*cli.Command{
			{
				Name:  "add",
				Usage: "add a new feed subscription",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "url",
						Usage:    "URL of the feed to add",
						Required: true,
					},
					&cli.BoolFlag{
						Name:  "fetch-content",
						Usage: "fetch content of items",
						Value: true,
					},
					&cli.StringFlag{
						Name:  "category",
						Usage: "category the feed and its items should be added to",
					},
					&cli.BoolFlag{
						Name:  "add-to-category",
						Usage: "add the feed to a category",
						Value: true,
					},
				},
				Action: func(c *cli.Context) error {
					var category *string
					if c.IsSet("category") {
						cat := c.String("category")
						category = &cat
					}
					return addFeed(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.Bool(flagDebug), c.String("url"), c.Bool("fetch-content"), c.Bool("add-to-category"), category)
				},
			},
			{
				Name:  "list",
				Usage: "list feed subscriptions",
				Action: func(c *cli.Context) error {
					return listFeeds(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.Bool(flagDebug))
				},
			},
			{
				Name:  "remove",
				Usage: "remove a feed subscription",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "url",
						Usage: "URL of the feed to remove. If not provided, the user will be prompted to select a feed to remove",
					},
				},
				Action: func(c *cli.Context) error {
					var feedURL *string
					if c.IsSet("url") {
						url := c.String("url")
						feedURL = &url
					}
					return removeFeed(c.Context, c.String(flagOmnivoreAPIKey), c.String(flagOmnivoreAPIURL), c.Bool(flagDebug), feedURL)
				},
			},
		},
	}
}
