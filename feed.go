package main

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/charmbracelet/huh"
	log "github.com/sirupsen/logrus"

	"gitlab.com/msvechla/omnivore-cli/pkg/omnivoreapi"
)

func listFeeds(ctx context.Context, apiKey string, endpoint string, debug bool) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}

	feeds, errCodes, err := client.Subscriptions(ctx, nil, nil)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("errors: %v", errCodes)
	}

	activeFeeds := filterFeedByStatus(feeds, omnivoreapi.SubscriptionStatusActive)

	d, err := json.MarshalIndent(activeFeeds, "", "  ")
	if err != nil {
		return err
	}
	fmt.Println(string(d))
	return nil
}

func addFeed(ctx context.Context, apiKey string, endpoint string, debug bool, feedURL string, fetchContent bool, addToCategory bool, category *string) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}

	sub, updated, err := client.EnsureSubscription(
		ctx,
		omnivoreapi.SubscribeInput{
			URL:              feedURL,
			SubscriptionType: omnivoreapi.RSS,
			AutoAddToLibrary: true,
			FetchContent:     fetchContent,
			IsPrivate:        false,
		},
	)
	if err != nil {
		return err
	}
	log.WithField("updated", updated).Info("ensured subscription to feed")

	if !addToCategory {
		return nil
	}

	categories, err := getCategoryLabels(ctx, client)
	if err != nil {
		return err
	}

	if category == nil {
		cat, promptErr := promptForCategory(fmt.Sprintf("Category for %s", sub.Name), categories)
		if promptErr != nil {
			return promptErr
		}
		category = &cat
	}

	feed := Feed{
		Title:  sub.Name,
		XMLURL: feedURL,
	}

	updated, err = createLabelRuleForFeedAndCategory(ctx, client, feed.ToRuleForCategory(*category))
	if err != nil {
		return err
	}
	log.WithField("updated", updated).Info("ensured label rule for feed")
	return err
}

func removeFeed(ctx context.Context, apiKey string, endpoint string, debug bool, feedURL *string) error {
	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return err
	}

	feeds, errCodes, err := client.Subscriptions(ctx, nil, nil)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("errors: %v", errCodes)
	}

	activeFeeds := filterFeedByStatus(feeds, omnivoreapi.SubscriptionStatusActive)

	if feedURL == nil {
		feedURL, err = promptForFeedURL("Select feed to remove", activeFeeds)
		if err != nil {
			return err
		}

		if feedURL == nil {
			return nil
		}
	}

	sub, err := client.GetSubscriptionFromCacheByURL(ctx, *feedURL)
	if err != nil {
		return err
	}

	id := omnivoreapi.ID(sub.ID)

	_, errCodesUnsub, errUnsub := client.Unsubscribe(ctx, sub.Name, &id)
	if errUnsub != nil {
		return errUnsub
	}
	if len(errCodesUnsub) > 0 {
		return fmt.Errorf("errors: %v", errCodesUnsub)
	}

	log.WithField("feed", sub.Name).Info("unsubscribed from feed")
	return nil
}

func filterFeedByStatus(feeds []omnivoreapi.Subscription, status omnivoreapi.SubscriptionStatus) []omnivoreapi.Subscription {
	filteredFeeds := []omnivoreapi.Subscription{}
	for _, feed := range feeds {
		if feed.Status == status {
			filteredFeeds = append(filteredFeeds, feed)
		}
	}
	return filteredFeeds
}

func promptForFeedURL(promptTitle string, feeds []omnivoreapi.Subscription) (*string, error) {
	feedURL := ""

	options := []huh.Option[string]{}
	for _, feed := range feeds {
		options = append(options, huh.NewOption(feed.Name, feed.URL))
	}

	form := huh.NewForm(
		huh.NewGroup(
			huh.NewSelect[string]().
				Title(promptTitle).
				Options(
					options...,
				).
				Value(&feedURL),
		),
	)
	if err := form.Run(); err != nil {
		log.WithError(err).Error("prompt failed")
		return nil, err
	}
	return &feedURL, nil
}

func promptForCategory(promptTitle string, categories map[string]omnivoreapi.Label) (string, error) {
	category := ""

	options := []huh.Option[string]{}
	for categoryName := range categories {
		options = append(options, huh.NewOption(categoryName, categoryName))
	}

	form := huh.NewForm(
		huh.NewGroup(
			huh.NewSelect[string]().
				Title(promptTitle).
				Options(
					options...,
				).
				Value(&category),
		),
	)
	if err := form.Run(); err != nil {
		return "", fmt.Errorf("prompt failed: %w", err)
	}
	return category, nil
}
