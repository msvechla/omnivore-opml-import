package main

import (
	"context"
	"fmt"
	"os"

	"github.com/kaorimatz/go-opml"

	log "github.com/sirupsen/logrus"

	"gitlab.com/msvechla/omnivore-cli/pkg/omnivoreapi"
)

func importOPMLFile(ctx context.Context, apiKey string, endpoint string, debug bool, file string, createCategories bool, labelColor string) error {
	// nolint: gosec
	reader, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("failed to open file: %w", err)
	}
	opmlData, err := opml.Parse(reader)
	if err != nil {
		return fmt.Errorf("failed to parse OPML: %w", err)
	}

	feeds, categories, rules, savedSearches := parseOPML(opmlData)
	log.Infof("importing %d feeds, %d rules, and %d saved searches", len(feeds), len(rules), len(savedSearches))
	log.WithField("categories", categories).Info("discovered categories")

	client, err := omnivoreapi.NewDefaultClient(apiKey, endpoint, debug)
	if err != nil {
		return fmt.Errorf("failed to create client: %w", err)
	}

	for _, feed := range feeds {
		feedLogger := log.WithFields(log.Fields{
			"feed": feed.Title,
			"url":  feed.XMLURL,
		})

		_, updated, err := client.EnsureSubscription(
			ctx,
			omnivoreapi.SubscribeInput{
				URL:              feed.XMLURL,
				SubscriptionType: omnivoreapi.RSS,
				AutoAddToLibrary: true,
				FetchContent:     true,
				IsPrivate:        false,
			},
		)
		if err != nil {
			feedLogger.WithError(err).Error("failed to subscribe")
			continue
		}
		feedLogger.WithField("updated", updated).Info("ensured subscription to feed")
	}

	if createCategories {
		for _, category := range categories {
			categoryLogger := log.WithField("category", category)
			if updated, err := createSavedSearchForCategory(ctx, client, category); err != nil {
				categoryLogger.WithError(err).Error("failed to set up saved search for category")
			} else {
				categoryLogger.WithField("updated", updated).Info("set up saved search for category")
			}

			desc := savedSearchFilterDescription(category)
			if _, updated, err := client.EnsureLabel(
				ctx,
				omnivoreapi.CreateLabelInput{
					Name:        feedCategoryLabel(category),
					Color:       &labelColor,
					Description: desc,
				}); err != nil {
				categoryLogger.WithError(err).Error("failed to set up label for category")
			} else {
				categoryLogger.WithField("updated", updated).Info("set up label for category")
			}
		}

		err := client.RefreshLabelsCache(ctx)
		if err != nil {
			return err
		}

		for _, rule := range rules {
			ruleLogger := log.WithField("rule", rule)
			if updated, err := createLabelRuleForFeedAndCategory(ctx, client, rule); err != nil {
				ruleLogger.Errorf("failed to set rule: %v", err)
			} else {
				ruleLogger.WithField("updated", updated).Info("ensured rule")
			}
		}
	}

	return nil
}
