// Package main is the entry point of the program
package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/kaorimatz/go-opml"
)

// RuleWhen is a type that represents when a rule should trigger
type RuleWhen int

const (
	// WhenPageCreated is a rule that triggers when a page is created
	WhenPageCreated RuleWhen = iota
	// WhenPageUpdated is a rule that triggers when a page is updated
	WhenPageUpdated
)

func (r RuleWhen) String() string {
	switch r {
	case WhenPageCreated:
		return "PAGE_CREATED"
	case WhenPageUpdated:
		return "PAGE_UPDATED"
	default:
		return "unknown"
	}
}

// Feed is a type that represents a feed in Omnivore
type Feed struct {
	Title  string
	XMLURL string
}

// ToRuleForCategory returns a rule for the feed in the given category
func (f Feed) ToRuleForCategory(category string) Rule {
	return Rule{
		Name:   fmt.Sprintf("feed-category-%s", f.Title),
		Filter: fmt.Sprintf("rss:\"%s\"", f.XMLURL),
		When:   WhenPageCreated,
		Labels: []string{feedCategoryLabel(category)},
	}
}

// Rule is a type that represents a rule in Omnivore
type Rule struct {
	Name   string
	Filter string
	When   RuleWhen
	Labels []string
}

// SavedSearch is a type that represents a saved search in Omnivore
type SavedSearch struct {
	Name  string
	Query string
}

func main() {
	app := setupCLI()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func parseOPML(input *opml.OPML) ([]Feed, []string, []Rule, []SavedSearch) {
	var feeds []Feed
	var rules []Rule
	var savedSearches []SavedSearch
	var categories []string

	for _, outline := range input.Outlines {
		if isCategory(outline) {
			category := outline.Title
			categories = append(categories, category)
			savedSearch := createSavedSearchForCategoryLabel(category)
			savedSearches = append(savedSearches, savedSearch)

			for _, child := range outline.Outlines {
				if child.XMLURL != nil {
					feed := Feed{
						Title:  child.Title,
						XMLURL: child.XMLURL.String(),
					}
					feeds = append(feeds, feed)
					rules = append(rules, feed.ToRuleForCategory(category))
				}
			}
		}
	}
	return feeds, categories, rules, savedSearches
}

// createSavedSearchForCategoryLabel creates a saved search for the given category label
func createSavedSearchForCategoryLabel(category string) SavedSearch {
	return SavedSearch{
		Name:  category,
		Query: fmt.Sprintf("label:\"%s\"", category),
	}
}

func isCategory(outline *opml.Outline) bool {
	return outline.XMLURL == nil && outline.Title != ""
}
