// Package omnivoreapi provides a client for the Omnivore API
package omnivoreapi

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/hasura/go-graphql-client"
	log "github.com/sirupsen/logrus"
)

// SortParams represents parameters for sorting
type SortParams struct {
	Order SortOrder `json:"order"`
	By    SortBy    `json:"by"`
}

// SortBy represents the criteria by which to sort
type SortBy string

// ID is a type that represents an ID
type ID string

const (
	// SortByUpdatedTime indicates sorting by updated time
	SortByUpdatedTime SortBy = "UPDATED_TIME"
	// SortByScore indicates sorting by score
	SortByScore SortBy = "SCORE"
	// SortBySavedAt indicates sorting by saved time
	SortBySavedAt SortBy = "SAVED_AT"
	// SortByPublishedAt indicates sorting by published time
	SortByPublishedAt SortBy = "PUBLISHED_AT"
)

// SortOrder represents the order of sorting
type SortOrder string

const (
	// SortOrderAscending indicates sorting in ascending order
	SortOrderAscending SortOrder = "ASCENDING"
	// SortOrderDescending indicates sorting in descending order
	SortOrderDescending SortOrder = "DESCENDING"
)

// retryBackoff are the backoff times for retries
var retryBackoff = []time.Duration{
	1 * time.Second,
	2 * time.Second,
	4 * time.Second,
}

// authorizationHeaderTransport is an HTTP transport that adds an authorization header to requests
type authorizationHeaderTransport struct {
	apiKey    string
	transport http.RoundTripper
}

func newAuthorizationHeaderTransport(apiKey string) *authorizationHeaderTransport {
	return &authorizationHeaderTransport{
		apiKey:    apiKey,
		transport: http.DefaultTransport,
	}
}

func (t *authorizationHeaderTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.Header == nil {
		req.Header = make(http.Header)
	}
	req.Header.Set("authorization", t.apiKey)
	return t.transport.RoundTrip(req)
}

// Client is the Omnivore API client
type Client struct {
	graphql            *graphql.Client
	labelsCache        *map[string]Label
	filtersCache       *map[string]Filter
	rulesCache         *map[string]Rule
	subscriptionsCache *map[string]Subscription
}

// NewDefaultClient creates a new default client
func NewDefaultClient(apiKey string, endpoint string, debug bool) (*Client, error) {
	client := &Client{
		graphql: graphql.NewClient(
			endpoint,
			&http.Client{
				Timeout:   10 * time.Second,
				Transport: newAuthorizationHeaderTransport(apiKey),
			},
		).WithDebug(debug),
	}
	return client, nil
}

// isRateLimitError returns true if the error is a rate limit error
func isRateLimitError(err error) bool {
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), "429 Too Many Requests")
}

// EnsureWithRateLimitRetry retries a function when it hits a rate limit
func EnsureWithRateLimitRetry[T any](ctx context.Context, f func(ctx context.Context) (T, bool, error)) (T, bool, error) {
	for i := 0; i < len(retryBackoff); i++ {
		data, updated, err := f(ctx)
		if err != nil {
			if !isRateLimitError(err) {
				return data, updated, err
			}
			timeout := retryBackoff[i]
			log.Infof("hit api rate limit, retrying in %s...", timeout)
			time.Sleep(timeout)
			continue
		}
		return data, updated, nil
	}
	return f(ctx)
}
