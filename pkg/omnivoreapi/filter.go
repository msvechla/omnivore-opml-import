package omnivoreapi

import (
	"context"
	"fmt"
	"time"
)

// QueryFilters is the GraphQL query for getting filters
type QueryFilters struct {
	Filters FiltersResult `graphql:"filters"`
}

// MutationSaveFilter is the GraphQL mutation for saving a filter
type MutationSaveFilter struct {
	SaveFilter struct {
		SaveFilterResult
	} `graphql:"saveFilter(input: $input)"`
}

// FiltersSuccess is the success result of the filters query
type FiltersSuccess struct {
	Filters []Filter `json:"filters"`
}

// FiltersErrorCode represents error codes for the filters query
type FiltersErrorCode string

const (
	// FiltersErrUnauthorized indicates unauthorized error for the filters query
	FiltersErrUnauthorized FiltersErrorCode = "UNAUTHORIZED"
	// FiltersErrBadRequest indicates bad request error for the filters query
	FiltersErrBadRequest FiltersErrorCode = "BAD_REQUEST"
)

// FiltersError represents errors in the filters query
type FiltersError struct {
	ErrorCodes []FiltersErrorCode `json:"errorCodes"`
}

// FiltersResult represents the result of the filters query, either success or error
type FiltersResult struct {
	Success FiltersSuccess `graphql:"... on FiltersSuccess"`
	Error   FiltersError   `graphql:"... on FiltersError"`
}

// SaveFilterInput is the input for the saveFilter mutation
type SaveFilterInput struct {
	Name        string          `json:"name"`
	Filter      string          `json:"filter"`
	Folder      *string         `json:"folder,omitempty"`
	Description *string         `json:"description,omitempty"`
	Position    *int            `json:"position,omitempty"`
	Category    *FilterCategory `json:"category,omitempty"`
}

// Filter is a filter
type Filter struct {
	ID            string          `json:"id"`
	Name          string          `json:"name"`
	Filter        string          `json:"filter"`
	Position      int             `json:"position"`
	Folder        *string         `json:"folder,omitempty"`
	Description   *string         `json:"description,omitempty"`
	CreatedAt     time.Time       `json:"createdAt"`
	UpdatedAt     *time.Time      `json:"updatedAt,omitempty"`
	DefaultFilter bool            `json:"defaultFilter"`
	Visible       bool            `json:"visible"`
	Category      *FilterCategory `json:"category,omitempty"`
}

// FilterCategory is a filter category
type FilterCategory string

const (
	// FilterCategorySearch is the search filter category
	FilterCategorySearch FilterCategory = "Search"
)

// SaveFilterSuccess is the success result of the saveFilter mutation
type SaveFilterSuccess struct {
	Filter Filter `json:"filter"`
}

// SaveFilterError is the error result of the saveFilter mutation
type SaveFilterError struct {
	ErrorCodes []SaveFilterErrorCode `json:"errorCodes"`
}

// SaveFilterResult is the result of the saveFilter mutation
type SaveFilterResult struct {
	Success SaveFilterSuccess `graphql:"... on SaveFilterSuccess"`
	Error   SaveFilterError   `graphql:"... on SaveFilterError"`
}

// SaveFilterErrorCode is the error code of a saveFilter error
type SaveFilterErrorCode string

const (
	// SaveFilterErrUnauthorized is the unauthorized error code
	SaveFilterErrUnauthorized SaveFilterErrorCode = "UNAUTHORIZED"
	// SaveFilterErrBadRequest is the bad request error code
	SaveFilterErrBadRequest SaveFilterErrorCode = "BAD_REQUEST"
	// SaveFilterErrNotFound is the not found error code
	SaveFilterErrNotFound SaveFilterErrorCode = "NOT_FOUND"
)

// SaveFilter saves a filter
func (c *Client) SaveFilter(ctx context.Context, input SaveFilterInput) (Filter, []SaveFilterErrorCode, error) {
	var mutation MutationSaveFilter
	vars := map[string]interface{}{
		"input": input,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Filter{}, nil, err
	}
	if len(mutation.SaveFilter.Error.ErrorCodes) > 0 {
		return Filter{}, mutation.SaveFilter.Error.ErrorCodes, nil
	}
	return mutation.SaveFilter.Success.Filter, nil, nil
}

// EnsureFilter ensures a filter exists
// returns true if the filter was updated
func (c *Client) EnsureFilter(ctx context.Context, input SaveFilterInput) (*Filter, bool, error) {
	if filter, err := c.GetFilterFromCache(ctx, input.Name); err == nil {
		return &filter, false, nil
	}
	return EnsureWithRateLimitRetry(ctx, func(ctx context.Context) (*Filter, bool, error) {
		filter, errCodes, err := c.SaveFilter(ctx, input)
		if err != nil {
			return nil, false, err
		}
		if len(errCodes) > 0 {
			return nil, false, fmt.Errorf("failed to save filter: %v", errCodes)
		}
		return &filter, true, nil
	})
}

// GetFilters gets filters
func (c *Client) GetFilters(ctx context.Context) ([]Filter, []FiltersErrorCode, error) {
	var query QueryFilters
	if err := c.graphql.Query(ctx, &query, nil); err != nil {
		return nil, nil, err
	}
	if len(query.Filters.Error.ErrorCodes) > 0 {
		return nil, query.Filters.Error.ErrorCodes, nil
	}
	return query.Filters.Success.Filters, nil, nil
}

// RefreshFiltersCache refreshes the filters cache
func (c *Client) RefreshFiltersCache(ctx context.Context) error {
	filters, errCodes, err := c.GetFilters(ctx)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("failed to refresh filters cache: %v", errCodes)
	}
	c.filtersCache = &map[string]Filter{}
	for _, filter := range filters {
		(*c.filtersCache)[filter.Name] = filter
	}
	return nil
}

// GetFilterFromCache gets a filter from the cache
func (c *Client) GetFilterFromCache(ctx context.Context, name string) (Filter, error) {
	if c.filtersCache == nil {
		err := c.RefreshFiltersCache(ctx)
		if err != nil {
			return Filter{}, err
		}
	}
	filter, exists := (*c.filtersCache)[name]
	if !exists {
		return Filter{}, fmt.Errorf("filter not found in cache")
	}
	return filter, nil
}
