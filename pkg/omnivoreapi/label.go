package omnivoreapi

import (
	"context"
	"fmt"
)

// Label is a label
type Label struct {
	ID          ID      `json:"id"`
	Name        string  `json:"name"`
	Color       *string `json:"color,omitempty"`
	Description *string `json:"description,omitempty"`
}

// QueryLabels is the GraphQL query for getting labels
type QueryLabels struct {
	Labels LabelsResult `graphql:"labels"`
}

// LabelsSuccess is the success result of the labels query
type LabelsSuccess struct {
	Labels []Label `json:"labels"`
}

// LabelsErrorCode is the error code of a labels error
type LabelsErrorCode string

const (
	// LabelsErrUnauthorized is the unauthorized error
	LabelsErrUnauthorized LabelsErrorCode = "UNAUTHORIZED"
	// LabelsErrBadRequest is the bad request error
	LabelsErrBadRequest LabelsErrorCode = "BAD_REQUEST"
	// LabelsErrNotFound is the not found error
	LabelsErrNotFound LabelsErrorCode = "NOT_FOUND"
)

// LabelsError is the error result of the labels query
type LabelsError struct {
	ErrorCodes []LabelsErrorCode `json:"errorCodes"`
}

// LabelsResult is the result of the labels query
type LabelsResult struct {
	Success LabelsSuccess `graphql:"... on LabelsSuccess"`
	Error   LabelsError   `graphql:"... on LabelsError"`
}

// MutationCreateLabel is the GraphQL mutation for creating a label
type MutationCreateLabel struct {
	CreateLabel struct {
		CreateLabelResult
	} `graphql:"createLabel(input: $input)"`
}

// MutationDeleteLabel is the GraphQL mutation for deleting a label
type MutationDeleteLabel struct {
	DeleteLabel struct {
		DeleteLabelResult
	} `graphql:"deleteLabel(id: $id)"`
}

// CreateLabelInput is the input for the createLabel mutation
type CreateLabelInput struct {
	Name        string  `json:"name"`
	Color       *string `json:"color,omitempty"`
	Description *string `json:"description,omitempty"`
}

// CreateLabelSuccess is the success result of the createLabel mutation
type CreateLabelSuccess struct {
	Label Label `json:"label"`
}

// CreateLabelErrorCode is the error code of a createLabel error
type CreateLabelErrorCode string

// DeleteLabelSuccess represents the success result of deleting a label
type DeleteLabelSuccess struct {
	Label Label `json:"label"`
}

// DeleteLabelErrorCode represents error codes for deleting a label
type DeleteLabelErrorCode string

const (
	// DeleteLabelErrUnauthorized indicates unauthorized error for deleting a label
	DeleteLabelErrUnauthorized DeleteLabelErrorCode = "UNAUTHORIZED"
	// DeleteLabelErrBadRequest indicates bad request error for deleting a label
	DeleteLabelErrBadRequest DeleteLabelErrorCode = "BAD_REQUEST"
	// DeleteLabelErrNotFound indicates not found error for deleting a label
	DeleteLabelErrNotFound DeleteLabelErrorCode = "NOT_FOUND"
	// DeleteLabelErrForbidden indicates forbidden error for deleting a label
	DeleteLabelErrForbidden DeleteLabelErrorCode = "FORBIDDEN"
)

// DeleteLabelError represents errors in deleting a label
type DeleteLabelError struct {
	ErrorCodes []DeleteLabelErrorCode `json:"errorCodes"`
}

// DeleteLabelResult represents the result of deleting a label, either success or error
type DeleteLabelResult struct {
	Success DeleteLabelSuccess `graphql:"... on DeleteLabelSuccess"`
	Error   DeleteLabelError   `graphql:"... on DeleteLabelError"`
}

const (
	// CreateLabelErrUnauthorized is the unauthorized error
	CreateLabelErrUnauthorized CreateLabelErrorCode = "UNAUTHORIZED"
	// CreateLabelErrBadRequest is the bad request error
	CreateLabelErrBadRequest CreateLabelErrorCode = "BAD_REQUEST"
	// CreateLabelErrNotFound is the not found error
	CreateLabelErrNotFound CreateLabelErrorCode = "NOT_FOUND"
	// CreateLabelErrAlreadyExists is the label already exists error
	CreateLabelErrAlreadyExists CreateLabelErrorCode = "LABEL_ALREADY_EXISTS"
)

// CreateLabelError is the error result of the createLabel mutation
type CreateLabelError struct {
	ErrorCodes []CreateLabelErrorCode `json:"errorCodes"`
}

// CreateLabelResult is the result of the createLabel mutation
type CreateLabelResult struct {
	Success CreateLabelSuccess `graphql:"... on CreateLabelSuccess"`
	Error   CreateLabelError   `graphql:"... on CreateLabelError"`
}

// RefreshLabelsCache refreshes the labels cache
func (c *Client) RefreshLabelsCache(ctx context.Context) error {
	labels, errCodes, err := c.GetLabels(ctx)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("failed to refresh label cache: %v", errCodes)
	}
	c.labelsCache = &map[string]Label{}
	for _, label := range labels {
		(*c.labelsCache)[label.Name] = label
	}
	return nil
}

// GetLabelFromCache gets a label from the cache
func (c *Client) GetLabelFromCache(ctx context.Context, name string) (Label, error) {
	if c.labelsCache == nil {
		err := c.RefreshLabelsCache(ctx)
		if err != nil {
			return Label{}, err
		}
	}
	label, exists := (*c.labelsCache)[name]
	if !exists {
		return Label{}, fmt.Errorf("label not found: %s", name)
	}
	return label, nil
}

// GetLabelsFromCache gets labels from the cache
func (c *Client) GetLabelsFromCache(ctx context.Context) (map[string]Label, error) {
	if c.labelsCache == nil {
		err := c.RefreshLabelsCache(ctx)
		if err != nil {
			return nil, err
		}
	}
	return *c.labelsCache, nil
}

// GetLabels gets labels
func (c *Client) GetLabels(ctx context.Context) ([]Label, []LabelsErrorCode, error) {
	var query QueryLabels
	if err := c.graphql.Query(ctx, &query, nil); err != nil {
		return nil, nil, err
	}
	if len(query.Labels.Error.ErrorCodes) > 0 {
		return nil, query.Labels.Error.ErrorCodes, nil
	}
	return query.Labels.Success.Labels, nil, nil
}

// CreateLabel creates a label
func (c *Client) CreateLabel(ctx context.Context, input CreateLabelInput) (Label, []CreateLabelErrorCode, error) {
	var mutation MutationCreateLabel
	vars := map[string]interface{}{
		"input": input,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Label{}, nil, err
	}
	if len(mutation.CreateLabel.Error.ErrorCodes) > 0 {
		return Label{}, mutation.CreateLabel.Error.ErrorCodes, nil
	}

	return mutation.CreateLabel.Success.Label, nil, nil
}

// EnsureLabel ensures that a label exists
// returns a bool indicating if the label was updated
func (c *Client) EnsureLabel(ctx context.Context, createLabelInput CreateLabelInput) (*Label, bool, error) {
	if label, err := c.GetLabelFromCache(ctx, createLabelInput.Name); err == nil {
		return &label, false, nil
	}

	return EnsureWithRateLimitRetry(ctx, func(ctx context.Context) (*Label, bool, error) {
		label, errCodes, err := c.CreateLabel(ctx, createLabelInput)
		if err != nil {
			return nil, false, err
		}
		if len(errCodes) > 0 {
			return nil, false, fmt.Errorf("failed to create label: %v", errCodes)
		}
		return &label, true, nil
	})
}

// DeleteLabel deletes a label
func (c *Client) DeleteLabel(ctx context.Context, id ID) (Label, []DeleteLabelErrorCode, error) {
	var mutation MutationDeleteLabel
	vars := map[string]interface{}{
		"id": id,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Label{}, nil, err
	}
	if len(mutation.DeleteLabel.Error.ErrorCodes) > 0 {
		return Label{}, mutation.DeleteLabel.Error.ErrorCodes, nil
	}

	return mutation.DeleteLabel.Success.Label, nil, nil
}
