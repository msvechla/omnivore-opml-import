package omnivoreapi

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

// QueryRules is the GraphQL query for getting rules
type QueryRules struct {
	Rules RulesResult `graphql:"rules"`
}

// MutationSetRule is the GraphQL mutation for setting a rule
type MutationSetRule struct {
	SetRule struct {
		SetRuleResult
	} `graphql:"setRule(input: $input)"`
}

// Rule is a rule
type Rule struct {
	ID         string          `json:"id"`
	Name       string          `json:"name"`
	Filter     string          `json:"filter"`
	Actions    []RuleAction    `json:"actions"`
	Enabled    bool            `json:"enabled"`
	CreatedAt  time.Time       `json:"createdAt"`
	UpdatedAt  *time.Time      `json:"updatedAt,omitempty"`
	EventTypes []RuleEventType `json:"eventTypes"`
}

// RuleAction is a rule action
type RuleAction struct {
	Type   RuleActionType `json:"type"`
	Params []string       `json:"params"`
}

// RuleActionType is a rule action type
type RuleActionType string

const (
	// RuleActionTypeAddLabel is the add label rule action type
	RuleActionTypeAddLabel RuleActionType = "ADD_LABEL"
	// RuleActionTypeArchive is the archive rule action type
	RuleActionTypeArchive RuleActionType = "ARCHIVE"
	// RuleActionTypeDelete is the delete rule action type
	RuleActionTypeDelete RuleActionType = "DELETE"
	// RuleActionTypeMarkAsRead is the mark as read rule action type
	RuleActionTypeMarkAsRead RuleActionType = "MARK_AS_READ"
	// RuleActionTypeSendNotification is the send notification rule action type
	RuleActionTypeSendNotification RuleActionType = "SEND_NOTIFICATION"
)

// RulesSuccess is the success result of the rules query
type RulesSuccess struct {
	Rules []Rule `json:"rules"`
}

// RulesError is the error result of the rules query
type RulesError struct {
	ErrorCodes []RulesErrorCode `json:"errorCodes"`
}

// RulesResult is the result of the rules query
type RulesResult struct {
	Success RulesSuccess `graphql:"... on RulesSuccess"`
	Error   RulesError   `graphql:"... on RulesError"`
}

// RulesErrorCode is the error code of a rules error
type RulesErrorCode string

const (
	// RulesErrUnauthorized is the unauthorized error
	RulesErrUnauthorized RulesErrorCode = "UNAUTHORIZED"
	// RulesErrBadRequest is the bad request error
	RulesErrBadRequest RulesErrorCode = "BAD_REQUEST"
)

// RuleEventType is a rule event type
type RuleEventType string

const (
	// RuleEventPageCreated is the page created rule event type
	RuleEventPageCreated RuleEventType = "PAGE_CREATED"
	// RuleEventPageUpdated is the page updated rule event type
	RuleEventPageUpdated RuleEventType = "PAGE_UPDATED"
)

// SetRuleInput is the input for the setRule mutation
type SetRuleInput struct {
	ID          *string           `json:"id,omitempty"`
	Name        string            `json:"name"`
	Description *string           `json:"description,omitempty"`
	Filter      string            `json:"filter"`
	Actions     []RuleActionInput `json:"actions"`
	Enabled     bool              `json:"enabled"`
	EventTypes  []RuleEventType   `json:"eventTypes"`
}

// RuleActionInput is a rule action input
type RuleActionInput struct {
	Type   RuleActionType `json:"type"`
	Params []string       `json:"params"`
}

// SetRuleSuccess is the success result of the setRule mutation
type SetRuleSuccess struct {
	Rule Rule `json:"rule"`
}

// SetRuleError is the error result of the setRule mutation
type SetRuleError struct {
	ErrorCodes []SetRuleErrorCode `json:"errorCodes"`
}

// SetRuleResult is the result of the setRule mutation
type SetRuleResult struct {
	Success SetRuleSuccess `graphql:"... on SetRuleSuccess"`
	Error   SetRuleError   `graphql:"... on SetRuleError"`
}

// SetRuleErrorCode is the error code of a setRule error
type SetRuleErrorCode string

const (
	// SetRuleErrNotFound is the not found error code
	SetRuleErrNotFound SetRuleErrorCode = "NOT_FOUND"
)

// DeleteRuleSuccess is the success result of the deleteRule mutation
type DeleteRuleSuccess struct {
	Rule Rule `json:"rule"`
}

// DeleteRuleError is the error result of the deleteRule mutation
type DeleteRuleError struct {
	ErrorCodes []DeleteRuleErrorCode `json:"errorCodes"`
}

// DeleteRuleResult is the result of the deleteRule mutation
type DeleteRuleResult struct {
	Success DeleteRuleSuccess `graphql:"... on DeleteRuleSuccess"`
	Error   DeleteRuleError   `graphql:"... on DeleteRuleError"`
}

// DeleteRuleErrorCode is the error code of a deleteRule error
type DeleteRuleErrorCode string

// SetRule sets a rule
func (c *Client) SetRule(ctx context.Context, input SetRuleInput) (*Rule, []SetRuleErrorCode, error) {
	var mutation MutationSetRule
	vars := map[string]interface{}{
		"input": input,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return nil, nil, err
	}
	if len(mutation.SetRule.Error.ErrorCodes) > 0 {
		return nil, mutation.SetRule.Error.ErrorCodes, nil
	}
	return &mutation.SetRule.Success.Rule, nil, nil
}

// RefreshRulesCache refreshes the rules cache
func (c *Client) RefreshRulesCache(ctx context.Context) error {
	rules, errCodes, err := c.GetRules(ctx)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("failed to refresh rules cache: %v", errCodes)
	}
	c.rulesCache = &map[string]Rule{}
	for _, rule := range rules {
		(*c.rulesCache)[rule.Name] = rule
	}
	return nil
}

// GetRuleFromCache gets a rule from the cache
func (c *Client) GetRuleFromCache(ctx context.Context, name string) (Rule, error) {
	if c.rulesCache == nil {
		err := c.RefreshRulesCache(ctx)
		if err != nil {
			log.Debugf("failed to refresh rules cache: %v", err)
			return Rule{}, err
		}
	}
	rule, exists := (*c.rulesCache)[name]
	if !exists {
		return Rule{}, fmt.Errorf("rule %s does not exist", name)
	}
	return rule, nil
}

// GetRulesFromCache gets rules from the cache
func (c *Client) GetRulesFromCache(ctx context.Context) (map[string]Rule, error) {
	if c.rulesCache == nil {
		err := c.RefreshRulesCache(ctx)
		if err != nil {
			return nil, err
		}
	}
	return *c.rulesCache, nil
}

// GetRules gets rules
func (c *Client) GetRules(ctx context.Context) ([]Rule, []RulesErrorCode, error) {
	var query QueryRules
	if err := c.graphql.Query(ctx, &query, nil); err != nil {
		return nil, nil, err
	}
	if len(query.Rules.Error.ErrorCodes) > 0 {
		return nil, query.Rules.Error.ErrorCodes, nil
	}
	return query.Rules.Success.Rules, nil, nil
}

// EnsureRule ensures a rule exists
// returns true if the rule was updated
func (c *Client) EnsureRule(ctx context.Context, input SetRuleInput) (*Rule, bool, error) {
	if rule, err := c.GetRuleFromCache(ctx, input.Name); err == nil {
		return &rule, false, nil
	}
	return EnsureWithRateLimitRetry(ctx, func(ctx context.Context) (*Rule, bool, error) {
		rule, errCodes, err := c.SetRule(ctx, input)
		if err != nil {
			return nil, false, err
		}
		if len(errCodes) > 0 {
			return nil, false, fmt.Errorf("failed to set rule: %v", errCodes)
		}
		return rule, true, nil
	})
}
