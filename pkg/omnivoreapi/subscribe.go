package omnivoreapi

import (
	"context"
	"fmt"
	"time"
)

// QuerySubscriptions is the GraphQL query for getting subscriptions
type QuerySubscriptions struct {
	Subscriptions struct {
		SubscriptionsResult
	} `graphql:"subscriptions(sort: $sort, type: $type)"`
}

// MutationUnsubscribe is the GraphQL mutation for unsubscribing from a feed
type MutationUnsubscribe struct {
	Unsubscribe struct {
		UnsubscribeResult
	} `graphql:"unsubscribe(name: $subscribtionName, subscriptionId: $subscriptionId)"`
}

// MutationSubscribe is the GraphQL mutation for subscribing to a feed
type MutationSubscribe struct {
	Subscribe struct {
		SubscribeResult
	} `graphql:"subscribe(input: $input)"`
}

// MutationUpdateSubscription is the GraphQL mutation for updating a subscription
type MutationUpdateSubscription struct {
	UpdateSubscription struct {
		UpdateSubscriptionResult
	} `graphql:"updateSubscription(input: $input)"`
}

// SubscribeResult is the result of the subscribe mutation
type SubscribeResult struct {
	Success SubscriptionsSuccess `graphql:"... on SubscribeSuccess"`
	Error   SubscriptionsError   `graphql:"... on SubscribeError"`
}

// SubscriptionsResult is the result of the subscriptions query
type SubscriptionsResult struct {
	Subscriptions SubscriptionsSuccess `graphql:"... on SubscriptionsSuccess"`
	Error         SubscriptionsError   `graphql:"... on SubscriptionsError"`
}

// SubscriptionsSuccess is the success result of the subscribe mutation
type SubscriptionsSuccess struct {
	Subscriptions []Subscription `json:"subscriptions"`
}

// SubscriptionsError is the error result of the subscribe mutation
type SubscriptionsError struct {
	ErrorCodes []SubscriptionsErrorCode `json:"errorCodes"`
}

// Subscription is a feed subscription
type Subscription struct {
	ID              string             `json:"id"`
	Name            string             `json:"name"`
	NewsletterEmail string             `json:"newsletterEmail"`
	URL             string             `json:"url"`
	Description     string             `json:"description"`
	Status          SubscriptionStatus `json:"status"`
	// Define other fields
}

// SubscriptionStatus is the status of a subscription
type SubscriptionStatus string

const (
	// SubscriptionStatusActive is the active status
	SubscriptionStatusActive SubscriptionStatus = "ACTIVE"
	// SubscriptionStatusUnsubscribed is the unsubscribed status
	SubscriptionStatusUnsubscribed SubscriptionStatus = "UNSUBSCRIBED"
	// SubscriptionStatusDeleted is the deleted status
	SubscriptionStatusDeleted SubscriptionStatus = "DELETED"
)

// SubscriptionsErrorCode is the error code of a subscription error
type SubscriptionsErrorCode string

const (
	// Unauthorized is the unauthorized error code
	Unauthorized SubscriptionsErrorCode = "UNAUTHORIZED"
	// BadRequest is the bad request error code
	BadRequest SubscriptionsErrorCode = "BAD_REQUEST"
	// NotFound is the not found error code
	NotFound SubscriptionsErrorCode = "NOT_FOUND"
	// AlreadySubscribed is the already subscribed error code
	AlreadySubscribed SubscriptionsErrorCode = "ALREADY_SUBSCRIBED"
	// ExceededMaxSubscriptions is the exceeded max subscriptions error code
	ExceededMaxSubscriptions SubscriptionsErrorCode = "EXCEEDED_MAX_SUBSCRIPTIONS"
)

// UnsubscribeResult is the result of the unsubscribe mutation
type UnsubscribeResult struct {
	Success UnsubscribeSuccess `graphql:"... on UnsubscribeSuccess"`
	Error   UnsubscribeError   `graphql:"... on UnsubscribeError"`
}

// UnsubscribeSuccess is the success result of the unsubscribe mutation
type UnsubscribeSuccess struct {
	Subscription Subscription `json:"subscription"`
}

// UnsubscribeError is the error result of the unsubscribe mutation
type UnsubscribeError struct {
	ErrorCodes []UnsubscribeErrorCode `json:"errorCodes"`
}

// UnsubscribeErrorCode is the error code of an unsubscribe error
type UnsubscribeErrorCode string

const (
	// UnsubscribeErrUnauthorized is the unauthorized error code
	UnsubscribeErrUnauthorized UnsubscribeErrorCode = "UNAUTHORIZED"
	// UnsubscribeErrBadRequest is the bad request error code
	UnsubscribeErrBadRequest UnsubscribeErrorCode = "BAD_REQUEST"
	// UnsubscribeErrNotFound is the not found error code
	UnsubscribeErrNotFound UnsubscribeErrorCode = "NOT_FOUND"
	// UnsubscribeErrAlreadyUnsubscribed is the already unsubscribed error code
	UnsubscribeErrAlreadyUnsubscribed UnsubscribeErrorCode = "ALREADY_UNSUBSCRIBED"
	// UnsubscribeErrMethodNotFound is the method not found error code
	UnsubscribeErrMethodNotFound UnsubscribeErrorCode = "UNSUBSCRIBE_METHOD_NOT_FOUND"
)

// SubscribeInput is the input for the subscribe mutation
type SubscribeInput struct {
	URL              string           `json:"url"`
	SubscriptionType SubscriptionType `json:"subscriptionType"`
	IsPrivate        bool             `json:"isPrivate"`
	AutoAddToLibrary bool             `json:"autoAddToLibrary"`
	FetchContent     bool             `json:"fetchContent"`
	Folder           string           `json:"folder"`
}

// SubscriptionType is the type of a subscription
type SubscriptionType string

const (
	// RSS is the RSS subscription type
	RSS SubscriptionType = "RSS"
	// Newsletter is the newsletter subscription type
	Newsletter SubscriptionType = "NEWSLETTER"
)

// UpdateSubscriptionInput represents input for updating a subscription
type UpdateSubscriptionInput struct {
	ID                  string              `json:"id"`
	Name                *string             `json:"name,omitempty"`
	Description         *string             `json:"description,omitempty"`
	LastFetchedChecksum *string             `json:"lastFetchedChecksum,omitempty"`
	Status              *SubscriptionStatus `json:"status,omitempty"`
	ScheduledAt         *time.Time          `json:"scheduledAt,omitempty"`
	IsPrivate           *bool               `json:"isPrivate,omitempty"`
	AutoAddToLibrary    *bool               `json:"autoAddToLibrary,omitempty"`
	FetchContent        *bool               `json:"fetchContent,omitempty"`
	FetchContentType    *FetchContentType   `json:"fetchContentType,omitempty"`
	Folder              *string             `json:"folder,omitempty"`
	RefreshedAt         *time.Time          `json:"refreshedAt,omitempty"`
	MostRecentItemDate  *time.Time          `json:"mostRecentItemDate,omitempty"`
	FailedAt            *time.Time          `json:"failedAt,omitempty"`
}

// FetchContentType represents the type of content to fetch for a subscription
type FetchContentType string

const (
	// FetchContentTypeFull indicates fetching full content
	FetchContentTypeFull FetchContentType = "FULL"
	// FetchContentTypePartial indicates fetching partial content
	FetchContentTypePartial FetchContentType = "PARTIAL"
)

// UpdateSubscriptionResult represents the result of updating a subscription
type UpdateSubscriptionResult struct {
	Success *UpdateSubscriptionSuccess `json:"success,omitempty"`
	Error   *UpdateSubscriptionError   `json:"error,omitempty"`
}

// UpdateSubscriptionSuccess represents the success result of updating a subscription
type UpdateSubscriptionSuccess struct {
	Subscription Subscription `json:"subscription"`
}

// UpdateSubscriptionError represents the error result of updating a subscription
type UpdateSubscriptionError struct {
	ErrorCodes []UpdateSubscriptionErrorCode `json:"errorCodes"`
}

// UpdateSubscriptionErrorCode represents error codes for updating a subscription
type UpdateSubscriptionErrorCode string

const (
	// UpdateSubscriptionErrUnauthorized indicates unauthorized error for updating a subscription
	UpdateSubscriptionErrUnauthorized UpdateSubscriptionErrorCode = "UNAUTHORIZED"
	// UpdateSubscriptionErrBadRequest indicates bad request error for updating a subscription
	UpdateSubscriptionErrBadRequest UpdateSubscriptionErrorCode = "BAD_REQUEST"
	// UpdateSubscriptionErrNotFound indicates not found error for updating a subscription
	UpdateSubscriptionErrNotFound UpdateSubscriptionErrorCode = "NOT_FOUND"
)

// RefreshSubscriptionsCache refreshes the subscriptions cache
func (c *Client) RefreshSubscriptionsCache(ctx context.Context) error {
	subscriptions, errCodes, err := c.Subscriptions(ctx, nil, nil)
	if err != nil {
		return err
	}
	if len(errCodes) > 0 {
		return fmt.Errorf("failed to refresh subscriptions cache: %v", errCodes)
	}
	c.subscriptionsCache = &map[string]Subscription{}
	for _, subscription := range subscriptions {
		(*c.subscriptionsCache)[subscription.URL] = subscription
	}
	return nil
}

// GetSubscriptionFromCacheByURL gets a subscription from the cache by URL
func (c *Client) GetSubscriptionFromCacheByURL(
	ctx context.Context,
	url string,
) (Subscription, error) {
	if c.subscriptionsCache == nil {
		if err := c.RefreshSubscriptionsCache(ctx); err != nil {
			return Subscription{}, err
		}
	}
	subscription, ok := (*c.subscriptionsCache)[url]
	if !ok {
		return Subscription{}, fmt.Errorf("subscription not found in cache")
	}
	return subscription, nil
}

// Subscriptions retrieves subscriptions
func (c *Client) Subscriptions(
	ctx context.Context,
	sort *SortParams,
	subscriptionType *SubscriptionType,
) ([]Subscription, []SubscriptionsErrorCode, error) {
	var query QuerySubscriptions
	vars := map[string]interface{}{
		"sort": sort,
		"type": subscriptionType,
	}
	if err := c.graphql.Query(ctx, &query, vars); err != nil {
		return nil, nil, err
	}
	if len(query.Subscriptions.Error.ErrorCodes) > 0 {
		return nil, query.Subscriptions.Error.ErrorCodes, nil
	}
	return query.Subscriptions.Subscriptions.Subscriptions, nil, nil
}

// Subscribe subscribes to a feed
func (c *Client) Subscribe(
	ctx context.Context,
	input SubscribeInput,
) (Subscription, []SubscriptionsErrorCode, error) {
	var mutation MutationSubscribe
	vars := map[string]interface{}{
		"input": input,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Subscription{}, nil, err
	}
	if len(mutation.Subscribe.Error.ErrorCodes) > 0 {
		return Subscription{}, mutation.Subscribe.Error.ErrorCodes, nil
	}
	return mutation.Subscribe.Success.Subscriptions[0], nil, nil
}

// Unsubscribe unsubscribes from a feed
func (c *Client) Unsubscribe(
	ctx context.Context,
	name string,
	subscriptionID *ID,
) (Subscription, []UnsubscribeErrorCode, error) {
	var mutation MutationUnsubscribe
	vars := map[string]interface{}{
		"subscribtionName": name,
		"subscriptionId":   subscriptionID,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Subscription{}, nil, err
	}
	if len(mutation.Unsubscribe.Error.ErrorCodes) > 0 {
		return Subscription{}, mutation.Unsubscribe.Error.ErrorCodes, nil
	}
	return mutation.Unsubscribe.Success.Subscription, nil, nil
}

// UpdateSubscription updates a subscription
func (c *Client) UpdateSubscription(
	ctx context.Context,
	input UpdateSubscriptionInput,
) (Subscription, []UpdateSubscriptionErrorCode, error) {
	var mutation MutationUpdateSubscription
	vars := map[string]interface{}{
		"input": input,
	}
	if err := c.graphql.Mutate(ctx, &mutation, vars); err != nil {
		return Subscription{}, nil, err
	}
	if len(mutation.UpdateSubscription.Error.ErrorCodes) > 0 {
		return Subscription{}, mutation.UpdateSubscription.Error.ErrorCodes, nil
	}
	return mutation.UpdateSubscription.Success.Subscription, nil, nil
}

// EnsureSubscription ensures a subscription exists
// returns true if the subscription was updated
func (c *Client) EnsureSubscription(
	ctx context.Context,
	input SubscribeInput,
) (*Subscription, bool, error) {
	if subscription, err := c.GetSubscriptionFromCacheByURL(ctx, input.URL); err == nil {
		return &subscription, false, nil
	}
	return EnsureWithRateLimitRetry(ctx, func(ctx context.Context) (*Subscription, bool, error) {
		subscription, errCodes, err := c.Subscribe(ctx, input)
		if err != nil {
			return nil, false, err
		}
		if len(errCodes) > 0 {
			if len(errCodes) == 1 && errCodes[0] == AlreadySubscribed {
				return nil, false, nil
			}
			return nil, false, fmt.Errorf("failed to subscribe: %v", errCodes)
		}
		return &subscription, true, nil
	})
}
